#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

export PATH=$PATH:$HOME/scripts
export EDITOR="nvim"

#export BROWSER=""

#if [[ "$(tty)" = "/dev/tty1" ]]; then
#	pgrep i3 || startx
#fi

# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# PATH
#export PATH=$PATH:$HOME/.config/scipts

#PS1='[\u@\h \W]\$ '
PS1='\h - \u \w \$ '
#PS1='\e[33;1m\u@\h: \e[31m\W\e[0m\$ '
#PS1='\e[s\e[0;0H\e[1;33m\h    \t\n\e[1;32mthese are words\e[u[\u@\h:  \w]\$ '

#colors
#LS_COLORS=$LS_COLORS:'d1=0;36'; export LS_COLORS
export LS_COLORS='rs=0:di=01;34:ln=01;36:mh=00:pi=40;33'

#defaults
export VISUAL="nvim"
export VK_ICD_FILENAMES="/usr/share/vulkan/icd.d/nvidia_icd.json"
export QT_QPA_PLATFORMTHEME="qt5ct"

#import
source ~/.config/.alias


# The next line updates PATH for the Google Cloud SDK.
if [ -f '/home/psurrena/google-cloud-sdk/path.bash.inc' ]; then . '/home/psurrena/google-cloud-sdk/path.bash.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/home/psurrena/google-cloud-sdk/completion.bash.inc' ]; then . '/home/psurrena/google-cloud-sdk/completion.bash.inc'; fi

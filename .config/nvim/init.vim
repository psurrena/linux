execute pathogen#infect()
filetype plugin indent on
autocmd StdinReadPre * let s:std_in=1
set number
set clipboard=unnamedplus
syntax on
set termguicolors
set shiftwidth=2
set tabstop=2
colorscheme nord 
"set t_Co=256
"set relativenumber

"start nerdtree
"autocmd VimEnter * NERDTree

let mapleader = "\<Space>"
let g:NERDTreeIgnore = ['node_modules']

"Disable arrow keys
inoremap  <Up>     <NOP>
inoremap  <Down>   <NOP>
inoremap  <Left>   <NOP>
inoremap  <Right>  <NOP>
noremap   <Up>     <NOP>
noremap   <Down>   <NOP>
noremap   <Left>   <NOP>
noremap   <Right>  <NOP>

inoremap	jj			<ESC>
inoremap	jk			<ESC>
inoremap	kj			<ESC>

nnoremap <Leader>w :w<CR>

"nerdtree remap
nmap <silent> <c-k> :wincmd k<CR>
nmap <silent> <c-j> :wincmd j<CR>
nmap <silent> <c-h> :wincmd h<CR>
nmap <silent> <c-l> :wincmd l<CR>

map <C-n> :NERDTreeToggle<CR>

"pandoc
"autocmd BufReadPost *.doc,*.docx,*.rtf,*.odp,*.odt silent %!pandoc "%" -tplain -o /dev/stdout

"COC
"source .coc_config

"CtrlP
"https://github.com/kien/ctrlp.vim
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_working_path_mode = 'ra'

"easyMotion
" <Leader>f{char} to move to {char}
map  <Leader>f <Plug>(easymotion-bd-f)
nmap <Leader>f <Plug>(easymotion-overwin-f)

" s{char}{char} to move to {char}{char}
nmap s <Plug>(easymotion-overwin-f2)

" Move to line
map <Leader>L <Plug>(easymotion-bd-jk)
nmap <Leader>L <Plug>(easymotion-overwin-line)

" Move to word
map  <Leader>w <Plug>(easymotion-bd-w)
nmap <Leader>w <Plug>(easymotion-overwin-w)

"eastMotion - Fuzzy Search
"function! s:config_easyfuzzymotion(...) abort
"  return extend(copy({
"  \   'converters': [incsearch#config#fuzzyword#converter()],
"  \   'modules': [incsearch#config#easymotion#module({'overwin': 1})],
"  \   'keymap': {"\<CR>": '<Over>(easymotion)'},
"  \   'is_expr': 0,
"  \   'is_stay': 1
"  \ }), get(a:, 1, {}))
"endfunction

"noremap <silent><expr> <Space>/ incsearch#go(<SID>config_easyfuzzymotion())

"NerdTree Custom Symbols
let g:NERDTreeGitStatusIndicatorMapCustom = {
	\ 'Modified'  :'✹',
	\ 'Staged'    :'✚',
	\ 'Untracked' :'✭',
	\ 'Renamed'   :'➜',
	\ 'Unmerged'  :'═',
	\ 'Deleted'   :'✖',
	\ 'Dirty'     :'✗',
	\ 'Ignored'   :'☒',
	\ 'Clean'     :'✔︎',
	\ 'Unknown'   :'?',
	\ }

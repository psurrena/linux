#!/usr/bin/env bash
# https://www.youtube.com/watch?v=7RNgpvBMua0&t=321s

killall -q polybar

while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

polybar example -r &

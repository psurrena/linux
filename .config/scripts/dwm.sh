#!/bin/bash
# based on: https://github.com/dbrgn/dotfiles/blob/master/.xsetroot.sh

title=""

function set_date {
    title+="$( date +"%b %d. %I:%M%p" )"
}

function set_ram {
    read -a fields <<< $(free -m | grep Mem)
		printf -v memfmt "%'d" ${fields[3]} 
		title+="m/${memfmt}GB"
}

function set_load {
    read -a fields <<< $(cat /proc/loadavg)	
    title+="c/${fields[0]}%"
}

function set_battery {
	btst=$( acpi -b | awk '/Battery 1/ {print $4}')
	title+="b/${btst}"
}

set_battery
title+=" - "
set_ram
title+=" - "
set_load
title+=" - "
set_date

xsetroot -name "$title"

#!/bin/sh

CHOICE=$(echo -e "4k\nHigh\nHighish\nMed\nNorm" | dmenu -i -p monitor )
DID="eDP-1"
#DID=DP-2

case ${CHOICE} in
	4k) xrandr --output ${DID}--mode 3840x2160 --rate 60 & ;;
	High) xrandr --output ${DID} --mode 2880x1620 --rate 60 & ;;
	Highish) xrandr --output ${DID} --mode 2560x1440 --rate 60 & ;;
	Med) xrandr --output ${DID} --mode 2048x1152 --rate 60 & ;;
	Norm) xrandr --output ${DID} --mode 1920x1080 --rate 60 & ;;
esac	
